<?php

namespace App\Helpers;

use File;
use Storage;
use Image;
use Illuminate\Support\Facades\Log;
class LaraHelpers
{

    /*
     *  upload file
     *
     *  @param string $filepath
     *  @param object/array $image_name
     *  @param mixed $unlink_image
     *  @param boolean $resize
     *  @return string $filename or $unlink_image
     * */
    public static function upload_image($filepath, $image_name, $unlink_image = '', $resize = false) {
        if($resize == true){
            $final_filepath = $filepath . 'thumb'. DIRECTORY_SEPARATOR;
        }else{
            $final_filepath = $filepath;
        }

        if (!is_dir($final_filepath)) {
            if(env('FILE_STORAGE') == 'Storage'){
                mkdir($final_filepath,0777,true);
            }else{
                File::makeDirectory($final_filepath, 755, true,true);
            }
        }

        /*if($resize == true){
            $thumbPath = $filepath . 'thumb'. DIRECTORY_SEPARATOR;
            if (!is_dir($thumbPath)) {
                if(env('FILE_STORAGE') == 'Storage'){
                    Storage::makeDirectory($thumbPath, 777, true,true);
                }else{
                    File::makeDirectory($thumbPath, 777, true,true);
                }
            }
        }*/
        $ftype = $image_name->getClientOriginalExtension();
        if ($image_name != "" && $ftype != 'pdf') {
            $file = $image_name;
            $extension = "";
            $extension = '.' . $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $file_name = time();
            $filename = $file_name.'_'.$name;






   //resized image from original
            $publicPath = $filepath;
//            $file->move($publicPath, $filename);
            //-----------

//            $fileName  = '';
//            $publicPath = $filepath;
//            $extension = $image_name->getClientOriginalExtension();
//            $filename = time() . '_' . $image_name->getClientOriginalName();

//            $image_name->move($publicPath,$filename);
            //$img = Image::make($publicPath.$filename);
            $img = Image::make($file->getRealPath());
            $width = $img->width();
            $height = $img->height();
            $ratio = 600;
            if($img->width() > $ratio ){
                if($height > $width){
                    $img->resize(null,$ratio, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }else{
                    $img->resize($ratio, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
            }
//print_r($filename);exit;
            $img->save($publicPath.$filename);

            //thumb
            if($resize == true){
                $thumbPath = $filepath . 'thumb'. DIRECTORY_SEPARATOR;
                $imges = Image::make($file->getRealPath());
                $ratio = 600;
                if($imges->width() > $ratio ){
                    if($height > $width){
                        $imges->resize(null,$ratio, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }else{
                        $imges->resize($ratio, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                }
//                $imges->resize(240, 180);
//                $imges->resize(240, null);
                $imges->save($thumbPath.$filename);
            }


            if (isset($unlink_image) && $unlink_image != "") {
                if($resize == true){
                    if(file_exists($thumbPath . $unlink_image)){
                        unlink($thumbPath . $unlink_image);
                    }
                }
                if(file_exists($filepath . $unlink_image)){
                    unlink($filepath . $unlink_image);
                }
            }
            return $filename;
        }else{
            $fileInfo = $image_name;
            $fname = time().'_'.$fileInfo-> getClientOriginalName();
            $fileInfo->move($filepath,$fname);
            //$fileInfo->save($filepath.$fname);
            if (isset($unlink_image) && $unlink_image != "") {
                if(file_exists($filepath . $unlink_image)){
                    unlink($filepath . $unlink_image);
                }
            }
            return $fname;
        }
        return $unlink_image;
    }

}