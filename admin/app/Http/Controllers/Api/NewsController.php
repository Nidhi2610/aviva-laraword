<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\Models\News;
use App\Models\AppUser;
use Illuminate\Support\Facades\Log;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $news;

    public function __construct(News $news)
    {
        $this->news = $news;
    }

    /**
     * Show the form for creating a new news.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNewsList(Request $request)
    {
        $notification_time = date('Y-m-d H:i:s',strtotime($request->notification_clear_at));
        $userData = AppUser::where('api_token',$request->api_token)->update(array('notification_clear_at'=>$notification_time));
        $newsData = $this->news->getNewsCollections();

        if($newsData){
            $filepath = url('/') . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'news' . DIRECTORY_SEPARATOR;
            $data = array();
            foreach($newsData as $row)
            {
                $data = $row->image = $filepath.$row->image;
            }
            return response(['statusCode' =>1,'data' =>$newsData,'message' => ['News List Retrieved']]);
        }else{

            return response(['statusCode' =>0,'message' => ['No News found']]);
        }

    }

    /**
     * get a description of given id
     * @param news_id
     *
     * @return \Illuminate\Http\Response
     */

    public function getNewsDetails(Request $request){
        $validator = Validator::make($request->all(), [
            'news_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response(['statusCode' =>0,'errors' => [],'message' =>$validator->errors()->all()]);
        }
        $data = $this->news->getNewsByField($request->news_id, 'id');
        Log::Info($data);

        if($data){
            $filepath = url('/') . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'news' . DIRECTORY_SEPARATOR;
            $data->image =  $filepath.$data->image;
            return response(['statusCode' =>1,'data' =>$data,'message' => ['News Detail Retrieved']]);
        }else{

            return response(['statusCode' =>0,'message' => ['No News found']]);
        }
    }


}
