@extends('admin.layouts.common')
@section('pageTitle')
    {{trans('app.client_add_title')}}
@endsection
@push('externalCssLoad')
@endpush
@push('internalCssLoad')

@endpush
@section('content')
    <div class="be-content">
        <div class="page-head">
            <h2>{{trans('app.client')}} {{trans('app.management')}}</h2>
            <ol class="breadcrumb">
                <li><a href="{{url('admin/dashboard')}}">{{trans('app.admin_home')}}</a></li>
                <li><a href="{{url('admin/client/list')}}">{{trans('app.client')}} {{trans('app.management')}}</a></li>
                <li class="active">{{trans('app.add')}} {{trans('app.client')}}</li>
            </ol>
        </div>
        <div class="main-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">{{trans('app.add')}} {{trans('app.client')}}</div>
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{url('admin/client/store')}}" enctype="multipart/form-data" name="app_add_form" id="app_form" style="border-radius: 0px;" method="post" class="form-horizontal group-border-dashed">
                                <input type="hidden" name="type" id="type"   value="1" />

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Client Name <span class="error">*</span></label>
                                    <div class="col-sm-6 col-md-4">
                                        <input type="text" name="name" id="name" placeholder="Client Name" class="form-control input-sm required" value="{{old('name')}}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Description <span class="error">*</span></label>
                                    <div class="col-sm-6 col-md-4">
                                        <textarea type="text" name="description" id="description" placeholder="Description" class="form-control input-sm required" >{{old('description')}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Client Type <span class="error">*</span></label>
                                    <div class="col-sm-6 col-md-4">
                                        <select name="type" id="type" placeholder="Country Name" class="form-control input-sm required">
                                            <option value="" selected="selected">Client Type</option>
                                            <option value="0">Private</option>
                                            <option value="1">Co-Operative Dairy</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Status<span class="error">*</span></label>
                                    <div class="col-sm-6 col-md-4">

                                        <div class="switch-button switch-button-lg">
                                            <input name="status" id="swt1" checked type="checkbox" value="1" />
                                            <span>
                                                 <label for="swt1"></label>
                                             </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Image <span class="error">*</span></label>
                                    <div class="col-sm-6 col-md-4">
                                        <input type="file" name="image" id="image" class="form-control input-sm required" onchange="GetFileSize()"/>
                                        <input type="hidden" name="old_image" id="old_image" class="form-control input-sm required" value="" />
                                        <div class="image_show_error" style="color:#cc5965;"></div>

                                    </div>
                                </div>

                                {{ csrf_field() }}

                                <div class="col-sm-6 col-md-8 savebtn">
                                    <p class="text-right">
                                        <button type="submit" class="btn btn-space btn-info btn-lg add_news">{{trans('app.add')}} {{trans('app.client')}}</button>
                                        <a href="{{url('admin/client/list')}}" class="btn btn-space btn-danger btn-lg">Cancel</a>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('externalJsLoad')

@endpush
@push('internalJsLoad')
<script type="text/javascript">
    app.validate.init();

    //check size of uploaded image

    function GetFileSize() {
        var ckeck = $("#image").siblings('#image-error');
        if(ckeck){
            $('#image-error').text('');
        }
        var html ='';
        $('.image_show_error').html(html);
        var fi = document.getElementById('image'); // GET THE FILE INPUT.

        // VALIDATE OR CHECK IF ANY FILE IS SELECTED.
        if (fi.files.length > 0) {
            // RUN A LOOP TO CHECK EACH SELECTED FILE.
            for (var i = 0; i <= fi.files.length - 1; i++) {
                var fsize = fi.files.item(i).size;      // THE SIZE OF THE FILE.
                var img_size = Math.round((fsize / (1024 * 1024)));
                if (img_size > 2) {
                    $('#image').val('');
                    var html = 'Please upload image less than size 2MB.';
                    $('.image_show_error').html(html);
                }
            }
        }
    }

    $('.image_show_error').val('');
    $('.add_news').click(function(){
        var ckeck = $("#image").siblings('#image-error');
        if(ckeck){ console.log('hui');
            var html ='';
            $(".image_show_error").html(html);
        }
        console.log(ckeck);
    });


</script>
@endpush