<?php

namespace App\Models;
use App\Models\AppUser;
use Illuminate\Database\Eloquent\Model;
use Auth;
use phpDocumentor\Reflection\Types\Boolean;
use PushNotification;
use Illuminate\Support\Facades\Log;

class Notification extends Model
{
    protected $table = 'tbl_notifications';
    protected $primaryKey = 'id';
    protected $fillable = [
        'notification_id', 'receiver_id', 'sender_id','message','created_by','updated_by','type',
    ];



    /**
     * Send Notification to user logged in
     * @param  string $type
     * @param array $data
     * @return Boolean
     */

    public function sendNotification($data,$type){

        $users = new AppUser();
        $loggedUser = $users->getLoggedUser();
        $notificationStatus = $this->addNotifications($loggedUser,$type,$data['message'],$data['sender_id']);
//        if(isset($data['sender_id']) && !empty($data['sender_id'])):
//            $notificationStatus = $this->addNotifications($loggedUser,$type,$data['message'],$data['sender_id']);
//        else:
//            $notificationStatus = $this->addNotifications($loggedUser,$type,$data['message']);
//        endif;
        $notificationIds = array();
        $image = $data['imageUrl'];

//        if( file_exists($data['imageUrl'])){
//            $image =  $data['imageUrl'];
//        }

        if($loggedUser){
            foreach ($loggedUser as $user){
                $notificationIds[] = PushNotification::Device($user->UserDevice->notification_id);
                $counter = $this->getCounterofNotification($user->notification_clear_at);
                $message = [
                    "title"=>$data['title'],
                    "counter"=>$counter,
                    "message"=>$data['message'],
                    "image" =>$image,
                    "type" => $type,
                    "timestamp"=>date('Y-m-d H:i:s')];

                $devices = PushNotification::DeviceCollection($notificationIds);
                Log::Info($message);
                $collection = PushNotification::app('avivaAndroid')
                    ->to($devices)
                    ->send($message);

                foreach ($collection->pushManager as $push) {
                    $response = $push->getAdapter()->getResponse();
                }
                return true;
            }
        }
    }

    /**
     * Add Notification Log
     * @param string $type
     * @param $data
     * @return boolean true | false
     */
    public function addNotifications($data,$type,$message,$sender_id="")
    {
        if(isset($data)){

            $notificationData = Notification::create([
                'type' => $type,
                'notification_id' => 0,
                'receiver_id' =>0,
                'sender_id' => $sender_id,
                'message' => $message,
            ]);
//            foreach($data as $UserData)
//            {
//                if(isset($sender_id) && !empty($sender_id)):
//                    $notificationData = Notification::create([
//                        'type' => $type,
//                        'notification_id' => $UserData->UserDevice->notification_id,
//                        'receiver_id' =>$UserData->id,
//                        'sender_id' => $sender_id,
//                        'message' => $message,
//                    ]);
//                else:
//                    $notificationData = Notification::create([
//                        'type' => $type,
//                        'notification_id' => $UserData->UserDevice->notification_id,
//                        'receiver_id' =>$UserData->id,
//                        'sender_id' => Auth::id(),
//                        'message' => $message,
//
//                    ]);
//                endif;
//            }

        }
    }

    /**
     *  get conut of  unreaded notifications
     * @param int $id  loggedin user
     * @return int $counter
     */
    public function getCounterofNotification($time){

        if( gettype($time) == 'NULL'){
            $time = date('Y-m-d H:i:s');
        }
       $co =  Notification::where('is_read',0)->where('created_at','>=',$time)->count();
       return $co;
    }

}
