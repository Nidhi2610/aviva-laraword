<?php

class JSON_API_User {
  
  var $id;          // Integer
  var $name;        // String
  var $first_name;  // String
  var $last_name;   // String
  var $email;       // String
  var $role_id;         // Integer
  var $mobile_number; // String
  var $password; // String
  var $status;   // Integer
  var $remember_token;   // String
  var $wpdb;
  // Note:
  //   JSON_API_Author objects can include additional values by using the
  //   author_meta query var.
  
  function JSON_API_User($id = null) {
    if ($id) {
      $this->id = (int) $id;
    } else {
      $this->id = (int) get_the_author_meta('ID');
    }
    $this->set_value('slug', 'user_nicename');
    $this->set_value('name', 'display_name');
    $this->set_value('first_name', 'first_name');
    $this->set_value('last_name', 'last_name');
    $this->set_value('nickname', 'nickname');
    $this->set_value('url', 'user_url');
    $this->set_value('description', 'description');
    $this->set_author_meta();
    //$this->raw = get_userdata($this->id);
  }
  
  function set_value($key, $wp_key = false) { // to be edit.
    if (!$wp_key) {
      $wp_key = $key;
    }
    $this->$key = get_the_author_meta($wp_key, $this->id);
  }

  function get_value($request){
      global $wpdb;
      $result = $wpdb->get_results($wpdb->prepare('SELECT * FROM users WHERE email = %s ', $request['email']));
      //$result = $wpdb->get_results($wpdb->prepare('SELECT * FROM users WHERE email = %s AND password = %s', $request['email'] , bcrypt($request['password'])));
      print_r($result);
    if($result){
      $status = 1;
    }else{
      $status = 0;
    }
    return json_encode($status);
  }
  
  function set_author_meta() {
    global $json_api;
    if (!$json_api->query->author_meta) {
      return;
    }
    $protected_vars = array(
      'user_login',
      'user_pass',
      'user_email',
      'user_activation_key'
    );
    $vars = explode(',', $json_api->query->author_meta);
    $vars = array_diff($vars, $protected_vars);
    foreach ($vars as $var) {
      $this->set_value($var);
    }
  }
}

?>
