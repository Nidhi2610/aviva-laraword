<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

get_header(); ?>
<div class="about_banner"> <img src="<?php echo get_template_directory_uri();?>/images/banner-product.png" alt="client-banner - Images">
    <div class="banner_text">
        <div class="container">
            <div class=" request_banner_head text-center">
                <?php $key= 'request_banner_head_text'; ?>
                <h2> <?php  echo get_post_meta( $post->ID, $key, true); ?></h2>
                <p><?php  echo get_post_meta( $post->ID, 'request_banner_head_description', true); ?> </p>
            </div>
        </div>
    </div>
</div>
<div class="breadcrumb_menu navbar ">
    <div class="container breadcumb-container">
        <div class="row">
            <div class="col-sm-12  col-xs-12">
                <div class="breadcrumb_nav devloper_menu_text  pull-right">
                    <?php   if ( function_exists( 'yoast_breadcrumb' ) ) {
                        yoast_breadcrumb();
                    }?>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<?php
$post = get_post($post->ID);
$title = $post->post_name;
get_template_part( 'template-parts/page', $title );
?>
<div class="our-impact fl">
    <div class="background fl">

        <div class="wrap">
            <div class="our-imact-details ">
                <h2>Our Impact</h2>
                <p>The only thing that matter to us is high quality products and attainment of total client satisfaction. </p>
            </div>
            <div class="equipment-save fl">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="unit-sold fl">
                            <?php $key= 'Units Sold'; ?>
                            <h2><?php  echo get_post_meta(10, $key, true); ?></h2>
                            <p>Units Sold</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="unit-sold fl">
                            <?php $key= 'Farmers Served'; ?>
                            <h2><?php  echo get_post_meta(10, $key, true); ?></h2>
                            <p>Farmers Served</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="unit-sold fl">
                            <?php $key= 'Litres of Milk Chilled'; ?>
                            <h2><?php  echo get_post_meta(10, $key, true); ?></h2>
                            <p>Litres of Milk Chilled</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="unit-sold fl">
                            <?php $key= 'Litres of Diesel Saved'; ?>
                            <h2><?php  echo get_post_meta(10, $key, true); ?></h2>
                            <p>Litres of Diesel Saved</p>
                        </div>
                    </div>
                </div>

            </div><!-- #equipment-save fl-->
        </div><!-- #wrap-->
    </div><!-- #background fl-->
</div><!-- #our-impact fl -->



<?php get_footer(); ?>
