<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header"> <br/>
					<h1 class="our-values"><?php _e( 'Oops! That page can&rsquo;t be found.', 'aviva' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content" style="text-align:center">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'aviva' ); ?></p>

					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- .site-main -->

		<?php //get_sidebar( 'content-bottom' ); ?>

	</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
