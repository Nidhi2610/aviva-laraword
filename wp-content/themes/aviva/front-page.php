<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

get_header(); ?>
<!--div class="owl-carousel owl-theme">
                <div class="item fl">
                        <img src="<?php echo get_template_directory_uri();?>/images/Home_Banner.jpg" alt="aviva-banner">
                        
                </div>
				<div class="item fl">
                        <img src="<?php echo get_template_directory_uri();?>/images/Home_Banner.jpg" alt="aviva-banner">
                        
                </div>
              
</div-->
<?php echo do_shortcode('[metaslider id=95]');?>

</div><!-----------<div class="banner"> div close-------------->
</div><!------------<div class="header fl"> div close---------->
<div class="section fl">
    <div class="wrap">
        <div class="our-details fl">
            <div class="row display-flex ">
                <?php $post = get_post( 30);?>
                <div class="col-sm-4 col-xs-12 display-inlineflex">
                    <div class="our-values">
                        <?php  $featured_img_url = get_the_post_thumbnail_url($page->ID); //echo $featured_img_url;?>
                        <img src="<?php echo $featured_img_url;?>" alt="our-value">
                        <h2><?php echo $post->post_title; ?></h2>
                        <p><?php echo excerpt_aviva(13);  ?></p>
                    </div>
                </div>
                <?php $post = get_post( 33);?>
                <div class="col-sm-4 col-xs-12 display-inlineflex">
                    <div class="our-values fl">
                       <?php  $featured_img_url = get_the_post_thumbnail_url($page->ID); //echo $featured_img_url;?>
                        <img src="<?php echo $featured_img_url;?>" alt="our-value">
                        <h2><?php echo $post->post_title; ?></h2>
                        <p><?php echo excerpt_aviva(13);  ?></p>
                    </div>
                </div>
                <?php $post = get_post( 35);?>
                <div class="col-sm-4 col-xs-12 display-inlineflex">
                    <div class="our-values">
                        <?php  $featured_img_url = get_the_post_thumbnail_url($page->ID); //echo $featured_img_url;?>
                        <img src="<?php echo $featured_img_url;?>" alt="our-value">
                        <h2><?php echo $post->post_title; ?></h2>
                        <p><?php echo excerpt_aviva(13);  ?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="aviva-equipments fl">
<!--             <div class="heading fl">
                <h2>Aviva Equipments</h2>
            </div> -->
<!--             <div class="map fl">
                <img src="<?php //echo get_template_directory_uri();?>/images/aviva-map.png" alt="aviva-map">
            </div> -->
<!--             <div class="equipments-details fl">
               <p><strong>Aviva Equipments Pvt. Ltd.: An ISO 9001:2008 Certified Company</strong> is associated with National Innovation Foundation-India, 
					which is an autonomous body of Department of Science and Technology of Government of
					India. With such strong bond with renowned firm, we got into formation in the year 2007. 
					We are applaud in this highly competitive marketplace as a distinguished <strong>exporter</strong>,
					<strong>manufacturer and exporter </strong>of <strong>Milking Machine, 
					Automatic Milking Machine, Milk Collecting Systems (AMCS), Bowl Type Weighing Scale,
					Bulk Milk Cooler Milk Measurement &amp; Monitoring System, Bulk Milk Coolers, Chaff Cutter, </strong>etc.
					<br/>We are fabricating these products with high precision using the premium grade components and spares for Dairy Industries.
					We offer the array in both standard and custom finishes, suiting the exact needs of the customers.
					
					<br/>
					<br/>
                <span>The main promoter of the company Mr. Bharat Shah is highly educated (B.<br>E. - Instrumentation and Control), from well-known Pune University
                    of India.<br>After graduation...</span>
                <a href="<?php// echo get_site_url(); ?>/about-us/">Read More</a>
            </div><!---------equipments-details  fl---------------> 
        </div> <!---------aviva-equipments fl--------------->
        <div class="product-ranges fl">
            <div class="head fl">
                <h2>Aviva Equipments Products Ranges</h2>
                <p>AVIVA EQUIPMENTS PRIVATE LIMITED is engaged in catering to the demands of technology savvy clientele with state of the art supplies<br>by
                    venturing in Manufacturing, Services as well as Domestic & International Trade on hi-tech grounds since 2007.</p>
            </div>
            <div class="product-info fl">
                <div class="row display-flex responsive_center">

                    <?php $results = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_machine WHERE status LIKE %d AND (deleted_at is null or deleted_at = "") LIMIT 8',1));
                          //$query="SELECT * FROM product_detail WHERE 1 AND status LIKE 'publish' LIMIT 0,5000";
                          //$results = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_machine WHERE app_dashboard LIKE %d AND status LIKE %d AND (deleted_at is null or deleted_at = "") LIMIT 4',1,1));
                          foreach($results as $result):
                              $base = get_site_url();
                              $machine_id = $result->id;
                              $query = "SELECT * FROM tbl_machine_images WHERE machine_id =" . $machine_id;
                              $image = $wpdb->get_col($query, 1);
                              $image_name = $image[0];
                               if($image_name){
                                  $url = $base."/admin/public/img/machine/".$machine_id."/thumb/".$image_name;
                                  $alt = $image_name;
                              }else{
                                  $url = $base."/admin/public/img/placeholder-image.png";
                                  $alt = "placeholder-image.png";
                              }
                     ?>
                    <div class="col-sm-3 display-inlineflex paddingtop">
                        <div class="milk-cooler fl">
                            <div class="cooler fl">
                                <img src="<?php echo $url;?>" alt="<?php echo $image_name;?> ">
                            </div>
                            <div class="details fl">
                                <h2><?php echo $result->machine_name;?></h2>
                                <p><?php
                                    $slug = sanitize_title_with_dashes($result->machine_name);
                                    echo my_trim_excerpt($result->description ,25,'');?></p>
                                <a href="<?php echo  get_site_url();?>/our-products/product-detail/<?php echo $machine_id;?>/?<?php echo $slug;?>">Read More <img src="<?php echo get_template_directory_uri();?>/images/read-more-icon.png" alt="icon"></a>
                            </div>
                        </div>
                    </div>
                    <?php
                         endforeach;
                    ?>


                </div>

            </div><!-----------product-info fl----------->
            <div class="view-products">
                <a href="<?php echo get_site_url();?>/our-products/">View All Products</a>
            </div>
        </div>
    </div>
</div>
<div class="our-impact fl">
    <div class="background fl">

        <div class="wrap">
            <div class="our-imact-details ">
                <h2>Our Impact</h2>
                <p>The only thing that matter to us is high quality products and attainment of total client satisfaction. </p>
            </div>
            <div class="equipment-save fl">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="unit-sold fl">
                            <?php $key= 'Units Sold'; ?>
                            <h2><?php  echo get_post_meta(10, $key, true); ?></h2>
                         <p>Units Sold</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="unit-sold fl">
                            <?php $key= 'Farmers Served';?>
                            <h2><?php  echo get_post_meta(10, $key, true); ?></h2>
                            <p>Farmers Served</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="unit-sold fl">
                            <?php $key= 'Litres of Milk Chilled'; ?>
                            <h2><?php  echo get_post_meta(10, $key, true); ?></h2>
                            <p>Litres of Milk Chilled</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="unit-sold fl">
                            <?php $key= 'Litres of Diesel Saved'; ?>
                            <h2><?php  echo get_post_meta(10, $key, true); ?></h2>
                            <p>Litres of Diesel Saved</p>
                        </div>
                    </div>
                </div>

            </div><!----------equipment-save fl----------->
        </div><!-------wrap------->
    </div><!------background fl------>
</div><!---------our-impact fl-------->
<?php get_footer(); ?>
