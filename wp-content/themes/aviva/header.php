<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?php echo get_template_directory_uri();?>/images/fevicon.ico" type="image/x-icon">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="<?php echo get_template_directory_uri();?>/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/css/custom-style.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<style>
		div.wpcf7-validation-errors{
			border: 0px;
			display:none;
		}
	</style>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >

<div class="header fl">
    <div class="header-top fl">
        <div class="wrap">
            <ul class="contactinfo nav nav-pills fl">
               <a href="https://play.google.com/store/apps/details?id=com.ms.aviva.eqpmnt&hl=en" target="_blank">
                   <li>
                    <img src="<?php echo get_template_directory_uri();?>/images/application-icon.png" alt="application-icon">Download Android Application
                   </li>
               </a>
            </ul>
            <ul class="social-icons nav navbar-nav fr">
                <li>
                    <img src="<?php echo get_template_directory_uri();?>/images/call-icon.png" alt="call-icon">24x7 Support <b>(+91)&nbsp</b>
                    <?php
                    if(is_active_sidebar('sidebar-4')){
                        dynamic_sidebar('sidebar-4');
                    }
                    ?>
                </li>
                <li><a href="mailto:aviva@avivaeuipments.com" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/email-icon.png" alt="e-mail-icon">
                        <?php
                        if(is_active_sidebar('sidebar-5')){
                            dynamic_sidebar('sidebar-5');
                        }
                        ?>
                </a></li>
            </ul>
        </div>
    </div><!--------header-top fl--------->
    <div class="modal fade form-modal" id="myModal" >
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><img src="<?php echo get_template_directory_uri();?>/images/close-button.png" alt="close-icon"></button>
                    <h2>Quick Enquiry</h2>
                </div>
                <div class="modal-body">
                    <?php echo do_shortcode('[contact-form-7 id="53" title="QUICK ENQUIRY"]');?>
                </div>
            </div>
        </div>



        <div class="modal fade sign-modal" id="sign-in" >
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="aviva-brand fl">
                            <div class="brand-logo">
                                <img src="<?php echo get_template_directory_uri();?>/images/aviva-logo.png" alt="aviva-logo">
                                <span>AN ISO 9001 - 2008 CERTIFIED COMPANY</span>
                            </div>
                        </div>
                        <div class="sign-form fl">
                            <button type="button" class="close" data-dismiss="modal"><img src="<?php echo get_template_directory_uri();?>/images/close-button.png" alt="close-icon"></button>
                            <span><img src="<?php echo get_template_directory_uri();?>/images/sign-in-form-icon.png" alt="sign-in-icon"></span>
                            <h3>Sign in</h3>
                            <form  action="javascript:void(0)" id="SigninFrom">
                                <div class="col-sm-12">
                                    <div class="form-inline">
                                        <div class="text fl">
                                            <input type="email" name="email" class="form-control" id="email" placeholder="Your Email" name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-inline">
                                        <div class="text fl">
                                            <input type="password" name="password" class="form-control" id="password" placeholder="Password" name="password">
                                        </div>
                                    </div>
                                </div>								
                                <div class="col-sm-6">
                                    <label class="control control--checkbox">Remember me
                                        <input type="checkbox" checked="checked"/>
                                        <div class="control__indicator"></div>
                                    </label>
                                </div>
                                <div class="col-sm-6">
                                    <!--a href="javascript:void(0)" class="Forget_modal" data-toggle="modal" data-dismiss="modal" data-target="#Forgot-Password">Forgot Password?</a-->
                                </div>
                                <div class="col-sm-12" id="errordiv">

                                </div>
                                <div class="moadl-sign-in col-sm-12 text-center">
                                    <button type="submit" id="sign-in-button" class="btn btn-default">Sign In</button>
                                </div>

                           </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade sign-modal" id="Forgot-Password" >
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="aviva-brand fl">
                            <div class="brand-logo">
                                <img src="<?php echo get_template_directory_uri();?>/images/aviva-logo.png" alt="aviva-logo">
                                <span>AN ISO 9001 - 2008 CERTIFIED COMPANY</span>
                            </div>
                        </div>
                        <div class="sign-form fl">
                            <button type="button" class="close" data-dismiss="modal"><img src="<?php echo get_template_directory_uri();?>/images/close-button.png" alt="close-icon"></button>
                            <span><img src="<?php echo get_template_directory_uri();?>/images/sign-in-form-icon.png" alt="sign-in-icon"></span>
                            <h3>Forgot Password</h3>
                            <p>Please enter your registered email id and press "Submit" <br>to retrive your password. Reset link will be sent to email <br>address on your account.</p>
                            <div class="col-sm-12">
                                <form class="form-inline">
                                    <div class="text fl">
                                        <input type="email" class="form-control" id="emailid" placeholder="Your Email" name="emailid">
                                    </div>
                                </form>
                            </div>
                            <div class="moadl-submit-in col-sm-12 text-center">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                            <div class="col-sm-12 text-center">
                                <a href="javascript:void(0)" data-toggle="modal" data-dismiss="modal" data-target="#sign-in" class="back-sign">Back to Sign in</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>


        <div  class="header-middel fl">
        <div class="wrap">
            <div class="logo fl">
                <?php aviva_the_custom_logo(); ?>
                <span><?php bloginfo('description'); ?></span>
            </div>

            <?php
            $base = get_site_url();
            $query = $wpdb->get_row($wpdb->prepare('SELECT file_name FROM tbl_brochure WHERE machine_id LIKE %d AND status LIKE %d AND type LIKE %d' , 0,1,0));
            $images = $query->file_name;
            $url = $base."/admin/public/img/brochure/".$images;
            ?>
            <?php $useragent = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
            $useragent_ipod = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
			  $useragent_ipad = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
            if(!$useragent || !$useragent_ipod || !$useragent_ipad) { ?>
                <div class="mobie_menu_btn fr">
                    <button class="btn menuBtn" id="openMenu"><span></span> <span></span> <span></span></button>
                </div>
                <div class="modal fade form-modal" id="signoutmessage">

                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close close_toggle" data-dismiss="modal"><img src="<?php echo get_template_directory_uri();?>/images/close-button.png" alt="close-icon"></button>

                            </div>
                            <div class="modal-body">
                                <h4>You are Successfully Signed Out!</h4>
                            </div>
                        </div>
                    </div>

                </div>
                <a class="sign-in fr" href="javascript:void(0)" data-toggle="modal" data-target="#sign-in">
                    <div class="sign_image" >
                        <img src="<?php echo get_template_directory_uri();?>/images/sign-in-icon.png" alt="sign-in-icon">
                        <h3><span class="hello">Hello</span><br><strong class="signin">Sign in</strong>
                            <strong class="signout" data-toggle="modal" data-target="#signoutmessage">Sign Out</strong>
                        </h3>
                    </div>
                </a>
                <a class="download-brochure fr" href="<?php echo $url;?>" target="_blank">
                    <div class="download_image" >
                        <img src="<?php echo get_template_directory_uri();?>/images/brochure-icon.png" alt="brochure-icon" style="">
                        <h3>Download<br><strong>Brochure</strong></h3>
                    </div>
                </a>
            <?php }else{ ?>
                <div class="mobie_menu_btn fr">
                    <button class="btn menuBtn" id="openMenu"><span></span> <span></span> <span></span></button>
                </div>
                <div class="modal fade form-modal" id="signoutmessage">

                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close close_toggle" data-dismiss="modal"><img src="<?php echo get_template_directory_uri();?>/images/close-button.png" alt="close-icon"></button>

                            </div>
                            <div class="modal-body">
                                <h4>You are Successfully Signed Out!</h4>
                            </div>
                        </div>
                    </div>

                </div>
                <style>
                    .download_image{    width: 50px;  height: 50px;padding: 10px 10px 27px 15px; }
                    img { max-width: 89%;}.download-brochure h3,.download-brochure h3 strong{font-size: 12px;}</style>
                <a class="download-brochure fr" href="<?php echo $url;?>" target="_blank">
                    <div class="download_image" >
                        <img src="<?php echo get_template_directory_uri();?>/images/brochure-icon.png" alt="brochure-icon" style="">
                        <h3>Download<br><strong>Brochure</strong></h3>
                    </div>
                </a>
            <?php } ?>





        </div><!------wrap------>
    </div><!---------header-middel fl---------->

    <div class="banner">
        <div id="menu-contain" class="header_main">
            <div class="wrap">
                <div class="header-bottom fl">
                    <?php if ( has_nav_menu( 'primary' ) ) : ?>
                    <nav class="main-navigation navbar  fl " id="site-navigation"  role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'aviva' ); ?>">


                                <?php
                                wp_nav_menu( array(
                                    'menu'          => 'Primary Menu',
                                    'menu_class'     => 'primary-menu nav navbar-nav',
                                    'container'      => 'container-fluid',
                                    'theme_location' => 'primary',
                                    'show_parent' => true,
                                    'sub_menu' => true,
                                ) );
                                ?>
                    </nav>
                    <?php endif;?>
                    <div class="inquiry fr">
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Quick Enquiry</button>
                    </div>
                </div><!-- #header-bottom fl---->
            </div><!-- #wrap-->
        </div><!-- #client-sec-->




