<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */
?>
<div class="clearfix"></div>
<div class="aboutus">
<div class="wrap">
    <div class="about-us fl">
        <div class="about-us-detail fl">
		   <?php the_content(); ?>            
        </div>
        <div class="aviva-values fl">
            <div class="map fl">
                <img src="<?php echo get_template_directory_uri();?>/images/aviva-map.png" alt="aviva-map">
            </div>
            <div class="value-details fl">
                <div class="value-heading">
                    <h2>Aviva Values</h2>
                </div>
                <?php the_excerpt(); ?>
				
            </div>
        </div>
        <div class="aviva-capabilities fl">
            <div class="head fl">
                <h2>Aviva Capabilities </h2>
				 <?php $key= 'Under Capabilities Description'; ?>
                            <p><?php  echo get_post_meta(12, $key, true); ?></p>
            </div>
            <div class="our-capabilities fl">
                <div class = "container">
                <div class="row">
                    <div class = "col-md-12">
                        <div class="img-cen">
                            <img src="<?php echo get_template_directory_uri();?>/images/about-us.png" width="250px">
                        </div>
                    </div>
                    <div class="col-md-4">
                      <?php
                        if(is_active_sidebar('sidebar-10')){
                            dynamic_sidebar('sidebar-10');
                        }
                        ?>
                       <!-- <div class="infrastructure-img">
                            <img src="<?php /*echo get_template_directory_uri();*/?>/images/Infrastructure-img.png" alt="infrastructure-img">
                        </div>-->
                    </div>

                    <div class="col-md-4">
                       <!-- <div class="research-wing-img">
                            <img src="<?php /*echo get_template_directory_uri();*/?>/images/Research-Wing-img.png" alt="infrastructure-img">
                        </div>-->
                        <?php
                        if(is_active_sidebar('sidebar-11')){
                            dynamic_sidebar('sidebar-11');
                        }
                        ?>
                    </div>


                    <div class="col-md-4">
                       <!-- <div class="paradigms-img fl">
                            <img src="<?php /*echo get_template_directory_uri();*/?>/images/Paradigms-img.png" alt="Paradigms-img">
                        </div>-->
                       <?php
                        if(is_active_sidebar('sidebar-12')){
                            dynamic_sidebar('sidebar-12');
                        }
                        ?>
                    </div>

                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    <div class="clearfix"></div>



