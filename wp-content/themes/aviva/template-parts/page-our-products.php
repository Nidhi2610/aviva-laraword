<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */
if($_GET['new_page']){
    $new_p =  $_GET['new_page'];
}
?>


<div class="product-sec fl">

    <div class="wrap">
        <div class="row">
            <div class="col-sm-3">
                <div class="paroduct-menu fl">
                    <div class="product-heading fl">
                        <h2>Our Products</h2>
                    </div>
                    <ul>

                    <?php
                    $result_category = $wpdb->get_results($wpdb->prepare('SELECT * FROM  tbl_category WHERE (deleted_at is null or deleted_at = "") AND status LIKE %d',1));
                        foreach ($result_category as $result){
                            $slug = sanitize_title_with_dashes($result->category_name);
                            echo '<li class="category_li" id="'.$result->id.'"><a href=" '. get_site_url().'/category-products?id='. $result->id .'&&'.$slug.'" id="category_name_'.$result->id.'" class="category_name"  >';
                            echo $result->category_name;
                            echo'<i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a>';
                        }
                    ?>
                    </li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-9">
                <div class="product-item fl">
                    <div class="row display-flex responsive_center" id="product_div">
                        <?php
                                 $result = $wpdb->get_results($wpdb->prepare('SELECT * FROM  tbl_machine WHERE (deleted_at is null or deleted_at = "")  AND  status LIKE %d',1));
                                 $size = sizeof( $result );
                                 if ($result) {
                                     $totalPages = ceil($size / 9);

                                     if (!$new_p) {
                                         $new_p = 1;
                                     } else {
                                     }
                                     $this_page_first_page = ($new_p - 1) * 9;
                                     $results = $wpdb->get_results($wpdb->prepare('SELECT * FROM  tbl_machine WHERE (deleted_at is null or deleted_at = "")  AND status LIKE %d  LIMIT 9 OFFSET %d',1,$this_page_first_page));
                            foreach ($results as $machine) :
                                         ?>

                                         <div class="col-sm-4  display-inlineflex">
                                             <div class="milk-cooler fl">
                                                 <div class="cooler fl">
                                                     <?php

                                                     $machine_id = $machine->id;
                                                     $images = $wpdb->get_row($wpdb->prepare('SELECT * FROM tbl_machine_images WHERE machine_id LIKE %d ', $machine_id));
                                                     $image_name =$images->image;
                                                     $base = get_site_url();
                                                     if($image_name){
                                                         $url = $base."/admin/public/img/machine/".$machine_id."/thumb/".$image_name;
                                                         $alt = $image_name;
                                                     }else{
                                                         $url = $base."/admin/public/img/placeholder-image.png";
                                                         $alt = "placeholder-image.png";
                                                     }
                                                     ?>
                                                     <img src='<?php echo $url; ?>' alt="<?php echo $image_name; ?>">
                                                 </div>
                                                 <div class="details fl">
                                                     <h2><?php echo $machine->machine_name; ?></h2>
                                                     <p><?php
                                                         $more = '<a href="#"> Read More <img src="' .$base . '/wp-content/themes/aviva/images/read-more-icon.png" alt="icon"></a>';
                                                         $mylongtext = $machine->description;
                                                         echo my_trim_excerpt($mylongtext, 10,'');
                                                         $slug = sanitize_title_with_dashes($machine->machine_name);
                                                         ?></p>

                                                     <a class="product-link" id="<?php echo $machine_id; ?>" href="<?php echo  get_site_url();?>/our-products/product-detail/<?php echo $machine_id;?>/?<?php echo $slug;?>">Read More <img src="<?php echo $base ; ?>/wp-content/themes/aviva/images/read-more-icon.png" alt="icon"></a>
                                                 </div>
                                             </div>
                                         </div>
                                        <?php
                                     endforeach;
                            $page++;
                        $limit_number = ($page - 1) * 9;
                                    ?>

                          <div class="pagination">
                                     <?php //$page = $page-1;
                                     if ($new_p <= 1) {
                                         echo "<span id='page_links' style='font-weight: bold;  padding-right:10px;'>Prev </span>";
                                     } else {
                                         $j = $new_p - 1;
                                         echo "<span ><a id='page_a_link' style='padding-right:10px;' href='?new_page=$j'> Prev</a></span>";
                                     }
                                     for ($i = 1; $i <= $totalPages; $i++) {
                                         if ($i != $new_p) {
                                             echo "<span class='page' style='padding-right:10px;padding-left:10px;'><a id='page_a_link' href='?new_page=$i'><b>$i</b></a></span>";
                                         } else {
                                             echo "<span style='padding-left:10px;padding-right:10px; font-weight: bold;' id='page_links'>$i</span>";
                                         }
                                     }
                                     if ($new_p == $totalPages) {
                                         echo "<span id='page_links' style='font-weight: bold;  padding-left:10px; padding-right:10px;'>Next </span>";
                                     } else {
                                         $j = $new_p + 1;
                                         echo "<span style=' padding-left:10px;padding-right:10px;' ><a id='page_a_link'  href='?new_page=$j'>Next</a></span>";
                                     }
                                 }
                                    ?>
                          </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<div class="clearfix"></div>


