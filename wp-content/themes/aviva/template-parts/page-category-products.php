<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

$parts = parse_url( $_SERVER['REQUEST_URI']);
$ex = explode('/',$parts['path']);
//$cat_slug = ($ex[count($ex)-1]);
if($_GET['id']){
    $id = explode('/',$_GET['id']);
    $cat_id = $id[0];
}
$base = get_site_url();
if($_GET['new_page']){
 $new_p =  $_GET['new_page'];
}
global  $catname_slug ;
?>
<div class="product-sec fl">

    <div class="wrap">
        <div class="row">
            <div class="col-sm-3">
                <div class="paroduct-menu fl">
                    <div class="product-heading fl">
                        <h2>Our Products</h2>
                    </div>
                    <ul>

                    <?php
                    $results_category = $wpdb->get_results($wpdb->prepare('SELECT * FROM  tbl_category WHERE status LIKE %d AND (deleted_at is null or deleted_at = "") ',1));
                        foreach ($results_category as $result_cat):

                        $slug= sanitize_title_with_dashes($result_cat->category_name);
                            echo '<li class="category_li" id="'.$result_cat->id.'">';
                            if($cat_id == $result_cat->id){
                                $catname_slug = $slug;
                               // echo '<a id="category_name_'.$result_cat->id.'" class="category_name custom-active" href=" '. get_site_url().'/category-products/'. $result_cat->id .'/?'.$slug.'">';
                                echo '<a id="category_name_'.$result_cat->id.'" class="category_name custom-active" href=" '. get_site_url().'/category-products?id='. $result_cat->id .'&&'.$slug.'">';
                            }else{
                               // echo '<a id="category_name_'.$result_cat->id.'" class="category_name" href=" '. get_site_url().'/category-products/'. $result_cat->id .'/?'.$slug.'">';
                                echo '<a id="category_name_'.$result_cat->id.'" class="category_name" href=" '. get_site_url().'/category-products?id='. $result_cat->id .'&&'.$slug.'">';
                            }

                            echo $result_cat->category_name;
                            echo'<i class="fa fa-angle-right pull-right" aria-hidden="true"></i></a> </li>';

                        endforeach;
                    ?>

                    </ul>
                </div>
            </div>

            <div class="col-sm-9">
                <div class="product-item ">
                    <div class="row display-flex responsive_center" id="product_div">
                        <?php
                        $result = $wpdb->get_results($wpdb->prepare('SELECT * FROM  tbl_machine WHERE category_id LIKE %d AND status LIKE %d AND (deleted_at is null or deleted_at = "")',$cat_id,1));
                        $size = sizeof( $result );
                    if ($result) {
                        $totalPages = ceil($size / 9);
                        if($totalPages == 1){
                            $new_page=0;
                        }else{
                            $new_page=1;
                        }

                        if (!$new_p) {
                            $new_p = 1;
                        } else {
                            $this_page_first_page = ($new_p - 1) * 9;
                        }

                        $results = $wpdb->get_results($wpdb->prepare('SELECT * FROM  tbl_machine WHERE (deleted_at is null or deleted_at = "") AND category_id LIKE %d AND status LIKE %d LIMIT 9 OFFSET %d',$cat_id,1,$this_page_first_page));
                        foreach ($results as $machine) :
                                  ?>
                                <div class="col-sm-4  display-inlineflex">
                                    <div class="milk-cooler fl">
                                        <div class="cooler fl">
                                            <?php

                                            $machine_id = $machine->id;
                                            $images = $wpdb->get_row($wpdb->prepare('SELECT * FROM tbl_machine_images WHERE machine_id LIKE %d ', $machine_id));
                                            $image_name =$images->image;

                                            if($image_name){
                                                $url = $base."/admin/public/img/machine/".$machine_id."/thumb/".$image_name;
                                                $alt = $image_name;
                                            }else{
                                                $url = $base."/admin/public/img/placeholder-image.png";
                                                $alt = "placeholder-image.png";
                                            }
                                            ?>
                                            <img src='<?php echo $url; ?>' alt="<?php echo $image_name; ?>">
                                        </div>
                                        <div class="details fl">
                                            <h2><?php echo $machine->machine_name; ?></h2>
                                            <p><?php
                                                $more = '<a href="#"> Read More <img src="' .$base . '/wp-content/themes/aviva/images/read-more-icon.png" alt="icon"></a>';
                                                $mylongtext = $machine->description;
                                                echo my_trim_excerpt($mylongtext, 10,'');
                                                $slug = sanitize_title_with_dashes($machine->machine_name);
                                                ?></p>

                                            <a class="product-link" id="<?php echo $machine_id; ?>" href="<?php echo  get_site_url();?>/our-products/product-detail/<?php echo $machine_id;?>/?<?php echo $slug;?>">Read More <img src="<?php echo $base ; ?>/wp-content/themes/aviva/images/read-more-icon.png" alt="icon"></a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            endforeach;
                        $limit_number = ($page - 1) * 9;

                    }else{?>
                      <div class="product-box product_not_available">
                            <img src='<?php echo $base ?>/wp-content/themes/aviva/images/product_not.jpg' alt="ProductNotAvailable">
                      </div>
                    <?php    }  ?>
                    </div>
                </div>

            </div>
        </div>

                          <?php if($new_page){?>
        <div class="row">
        <div class="col-sm-3">
        </div>
        <div class="col-sm-9">
                            <div class="pagination" >
                                <?php
                                if ($new_p <= 1) {
                                    echo "<span id='page_links' style='font-weight: bold;  padding-right:10px;'>Prev </span>";
                                } else {
                                    $j = $new_p - 1;
                                    echo "<span ><a id='page_a_link' style='padding-right:10px;' href='?id=$cat_id&&$catname_slug&&new_page=$j'> Prev</a></span>";
                                }
                                for ($i = 1; $i <= $totalPages; $i++) {
                                    if ($i != $new_p) {
                                        echo "<span class='page' style='padding-right:10px;padding-left:10px;'><a id='page_a_link' href='?id=$cat_id&&$catname_slug&&new_page=$i'><b>$i</b></a></span>";
                                    } else {
                                        echo "<span style='padding-left:10px;padding-right:10px; font-weight: bold;' id='page_links'>$i</span>";
                                    }
                                }
                                if ($new_p == $totalPages) {
                                    echo "<span id='page_links' style='font-weight: bold;  padding-left:10px; padding-right:10px;'>Next </span>";
                                } else {
                                    $j = $new_p + 1;
                                    echo "<span style=' padding-left:10px;padding-right:10px;' ><a id='page_a_link'  href='?id=$cat_id&&$catname_slug&&new_page=$j'>Next</a></span>";
                                } ?>

                            </div>
         </div>
         </div>
                          <?php }  else{
                              }
                          ?>




</div>
<div class="clearfix"></div>





