<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */
?>


<div class="clearfix"></div>
<div class="client-sec">
    <div class="container">
        <div class="row text-center">
          <?php  $images = $wpdb->get_results($wpdb->prepare('SELECT image FROM tbl_client WHERE (deleted_at is null or deleted_at = "") AND status LIKE %d',1));
          $base = get_site_url();
                    foreach($images as $image) :

                    if(sizeof($image)>0){
                        $url = $base."/admin/public/img/client/".$image->image;
                        $alt = $image->image;
                    }else{
                        $url =$base."/admin/public/img/placeholder-image.png";
                        $alt = "placeholder-image.png";
                    }
                    ?>
              <div class="Client_logo_box">
                   <div class="client-logo">
                       <img src="<?php echo $url;?>" alt="Client logo">
                   </div>
               </div>
           <?php endforeach;
           ?>
        </div><!-- #row text-center-->
    </div><!-- #container-->
</div><!-- #client-sec-->

<div class="clearfix"></div>



