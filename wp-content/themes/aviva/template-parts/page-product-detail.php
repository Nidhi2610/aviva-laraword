<?php
/**
 * The template used for displaying page content product detail
 *the content is fetched from "tbl_machine'
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

$parts = parse_url( $_SERVER['REQUEST_URI']);
$ex = explode('/',$parts['path']);
$id = ($ex[count($ex)-2]);
?>


<div class="product-sec fl">
<?php
$results = $wpdb->get_row($wpdb->prepare('SELECT * FROM tbl_machine WHERE id LIKE %d ' , $id));
?>
<div class="wrap">
    <div class="product_detail">
        <div class = "row">
            <div class="col-md-3">
            </div>
            <div class = "col-md-6">
                <div class = "product-summary">
                    <div class = "unit-sold">
                        <h2><?php echo $results->machine_name; ?> </h2>
                    </div>
                    <div class="owl-carousel owl-theme">
                        <?php
                        $machine_id = $results->id;
                        $images = $wpdb->get_results($wpdb->prepare('SELECT image FROM tbl_machine_images WHERE machine_id LIKE %d ', $machine_id));
                        $base = get_site_url();
                        if(sizeof($images)){
                        foreach($images as $image) : ?>
                            <div class="item">
                                <?php
                                $image_name = $image->image;
                                    $url = $base."/admin/public/img/machine/".$machine_id."/thumb/".$image_name;
                                    $alt = $image_name; ?>
                               <img src="<?php echo $url;?>" alt="<?php echo $results->machine_name;?>">
                            </div>
                            <?php
                        endforeach;
                        }else{
                            $url = $base."/admin/public/img/placeholder-image.png";
                            $alt = "placeholder-image.png"; ?>
                            <img src="<?php echo $url;?>" alt="<?php echo $alt;?>">
                            <?php
                        }
                        ?>

                    </div>
                    <div class="clearfix"></div>
                    <div class="product-detail">
                        <p><?php echo $results->description; ?> </p>
                    </div>
                    <div class="download-brochure_pro">
                        <?php
                        $query = $wpdb->get_row($wpdb->prepare('SELECT file_name FROM tbl_brochure WHERE machine_id LIKE %d AND status LIKE %d AND type LIKE %d' , $machine_id,1,1));
                        $images = $query->file_name;
                        $url = $base."/admin/public/img/brochure/".$images;
                        ?>
                        <?php if(!empty($query)) :?>
                        <a href="<?php echo $url;?>" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/download.png" width="40px;">  Download Brochure</a>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
            <div class="col-md-3">
            </div>

        </div>

    </div>
</div>
</div>
<div class="clearfix"></div>


