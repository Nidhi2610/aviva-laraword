<?php
/**
 * The template used for displaying page content
 * the content is fetched from "tbl_news'
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

?>
<script>
    jQuery(document).ready(function(){
        if(! localStorage.getItem('user')){
			var path = window.location.origin;
            window.location.href = path;
        }
    });
</script>

<div class="product-sec fl">

    <div class="wrap">
        <div class="news fl">
<!--            --><?php
//            $results = $wpdb->get_results($wpdb->prepare('SELECT * OM tbl_news  WHERE status LIKE %d ORDER BY id DESC ',1));
//            foreach($results as $result):
//
//            $base = get_site_url();
//            $image_name = $result->image;
//            if($image_name){
//            $url = $base."/admin/public/img/news/".$image_name;
//            $alt = $image_name;
//            }else{
//            $url = $base."/admin/public/img/placeholder-image.png";
//            $alt = "placeholder-image.png";
//            }
//            ?>
<!--            <div class="col-sm-6 display-inlineflex">-->
<!--                <div class="news-info fl">-->
<!--                    <div class="news-img fl">-->
<!--                        <img src="--><?php //echo $url; ?><!--" alt="--><?php //echo $alt; ?><!--">-->
<!--                    </div>-->
<!--                    <div class="news-details fl">-->
<!--                        <h2>--><?php //echo my_trim_title_excerpt ( $result->title ,5);
//                            $slug = sanitize_title_with_dashes($result->title);
//                        ?><!--</h2>-->
<!--                        <p>--><?php //echo my_trim_excerpt($result->description ,25,'');?><!--</p>-->
<!--                        <a href="--><?php //echo  get_site_url();?><!--/news/news-detail/--><?php //echo $result->id;?><!--/?--><?php //echo $slug;?><!--">Read More <img src="--><?php //echo get_template_directory_uri();?><!--/images/read-more-icon-1.png" alt="icon"></a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            --><?php // endforeach;   ?>


            <?php
            $results = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_news  WHERE (deleted_at is null or deleted_at = "")  AND status LIKE %d ORDER BY id DESC ',1));
            foreach($results as $result):
                $base = get_site_url();
                $image_name = $results->image;
                if($image_name){
                    $url = $base."/admin/public/img/news/".$image_name;
                    $alt = $image_name;
                }else{
                    $url = $base."/admin/public/img/placeholder-image.png";
                    $alt = "placeholder-image.png";
                }
                ?>
                <div class="col-sm-6 display-inlineflex">
                    <div class="news-info fl">
                        <div class="news-img fl">
                            <img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
                        </div>
                        <div class="news-details fl">
                            <h2><?php echo $result->title;?></h2>
                            <p><?php echo my_trim_excerpt($result->description ,25,'');
                                $slug = sanitize_title_with_dashes($result->title);
                            ?></p>
                            <a href="<?php echo  get_site_url();?>/news/news-detail/<?php echo $result->id;?>/?<?php echo $slug;?>">Read More <img src="<?php echo get_template_directory_uri();?>/images/read-more-icon-1.png" alt="icon"></a>
                        </div>
                    </div>
                </div>
            <?php  endforeach;   ?>


        </div>
    </div>
</div>

<div class="clearfix"></div>


