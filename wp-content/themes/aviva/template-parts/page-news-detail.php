<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */
$parts = parse_url( $_SERVER['REQUEST_URI']);
$ex = explode('/',$parts['path']);
$id = ($ex[count($ex)-2]);
?>
<script>
    jQuery(document).ready(function(){
        if(! localStorage.getItem('user')){
            var path = window.location.origin;	
            window.location.href = path;
        }
    });
</script>

<div class="product-sec fl">

    <?php
    $results = $wpdb->get_row($wpdb->prepare('SELECT * FROM tbl_news WHERE id LIKE %d',$id));
    ?>
    <div class="wrap">
        <div class="product_detail">
            <div class = "row">
                <div class="col-md-3">
                </div>
                <div class = "col-md-6">
                    <div class = "product-summary">
                        <div class = "unit-sold">
                            <h2><?php echo $results->title;?></h2>
					    </div>
					<div class = "product-image">
                        <div class="item">
                            <?php
                            $base = get_site_url();
                            $image_name = $results->image;
                            if($image_name){
                                $url = $base."/admin/public/img/news/".$image_name;
                                $alt = $image_name;
                            }else{
                                $url = $base."/admin/public/img/placeholder-image.png";
                                $alt = "placeholder-image.png";
                            }
                            ?>

                            <img src="<?php echo $url;?>" alt="<?php echo $results->title;?>">

					</div>
                    <div class="product-detail">
                        <p><?php echo $results->description; ?> </p>
                    </div>
				</div>
                </div>
                <div class="col-md-3">

                </div>

            </div>

        </div>
    </div>

</div>

<div class="clearfix"></div>


