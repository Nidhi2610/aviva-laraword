<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */
?>


<div class="contact-us">
    <div class="wrap">
        <div class="contact-head">
            <h2>Aviva Equipments Privete Limited</h2>
            <span>AN ISO 9001 - 2008 CERTIFIED COMPANY</span>
        </div>
        <div class="contact-details">
            <div class="office-add">
                <div class="office-img">
                    <a href="javascript:void(0)"><img src="<?php echo get_template_directory_uri();?>/images/office-icon.png" alt="office-icon"></a>
                </div>
                <div class="office-detail">
                    <?php //echo the_field('Office Address');?>
                    <h2>Office Address :</h2>
                    <p><?php
                        if(is_active_sidebar('sidebar-7')){
                            dynamic_sidebar('sidebar-7');
                        }
                        ?>
                    </p>
                </div>
            </div>
            <div class="factory-add">
                <div class="factory-img">
                    <a href="javascript:void(0)"><img src="<?php echo get_template_directory_uri();?>/images/factory-icon.png" alt="factory-icon"></a>
                </div>
                <div class="factory-detail">
                    <h2>Factory Address :</h2>
                    <p><?php
                        if(is_active_sidebar('sidebar-8')){
                            dynamic_sidebar('sidebar-8');
                        }
                        ?></p>
                </div>
            </div>
        </div>
        <div class="contact-info">
            <h2>Contact Person :</h2>
                <?php
                if(is_active_sidebar('sidebar-9')){
                    dynamic_sidebar('sidebar-9');
                }
                ?>
        </div>
    </div>
</div>


