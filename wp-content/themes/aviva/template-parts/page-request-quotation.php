<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */
//$nm = $_POST['user'];echo $nm;
//$user = array();
//$user = "<script>document.write(localStorage.getItem('user'));</script>";
//print_r($user->id); SELECT machine_id FROM tbl_quotation_category_machine


//$questions = $wpdb->get_results('SELECT machine_id,category_id FROM tbl_quotation_category_machine');
//$questions = $wpdb->get_results('SELECT DISTINCT category_id FROM tbl_quotation_category_machine');

$questions = $wpdb->get_results('SELECT DISTINCT category_id FROM tbl_quotation_category_machine LEFT JOIN tbl_category ON (tbl_quotation_category_machine.category_id = tbl_category.id) WHERE (tbl_category.deleted_at is null or tbl_category.deleted_at = "")');



foreach($questions as $q){
	 $cat =  $wpdb->get_col('SELECT  `category_name` FROM `tbl_category` WHERE `id` ='. $q->category_id.' AND `status`= 1 AND (deleted_at is null or deleted_at = "")');   
	
    //$cat = $wpdb->get_col('SELECT  `category_name` FROM `tbl_category` WHERE `id` ='. $q->category_id);

}
?>
<script>
    jQuery(document).ready(function(){
        if(! localStorage.getItem('user')){
            var path = window.location.origin;
            window.location.href = path;
        }
    });
</script>
<div class="request_question">
    <div class="request_wrap">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <ul class="request_menu">
                    <li ><span class="request_border" id="request_border"></span><div class="clearfix"></div>
                        <a  class="first_tab  active" data-toggle="tab" >1</a><label>Basic Information</label></li>
                    <li><span class="request_border"></span><div class="clearfix"></div><a class=" select_product_tab" data-toggle="tab">2</a><label>Select Product</label></li>
                    <li><span class="request_border"></span><div class="clearfix"></div><a  id="submit_request" data-toggle="tab" >3</a><label>Confirm & Submit</label></li>
                </ul>
            </div>
        </div>
        <div class="tab-content question_main">
            <div id="basic_info" class="tab-pane fade in active">
                <div class="row question_part">
                    <form id="question_tab">
                    <?php  $questions = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_question WHERE (deleted_at is null or deleted_at = "")  AND status LIKE %d',1));
                     foreach ($questions as $question): ?>

                        <?php if($question->type) : ?>
                            <div class="col-sm-12">
                                <div class="form-group question_answer">
                                    <label class="control-label" ><?php echo $question->question; ?> </label>
                                    <input type="text" name="question" class="form-control" placeholder="Enter Answer"  />
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="col-sm-6">
                                <div class="form-group question_answer">
                                    <label class="control-label" ><?php echo $question->question; ?> </label>
                                    <select name="question" class="form-control" placeholder="Enter Answer" >
                                        <option>Select Answer</option>
                                        <?php $options = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_question_option WHERE (deleted_at is null or deleted_at = "")  AND question_id LIKE %d',$question->id));
                                        foreach($options as $option): ?>
                                            <option><?php echo $option->option;?></option>
                                        <?php   endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        <?php endif;?>
                    <?php   endforeach;?>
                    <div class="col-sm-6">
                        <div class="form-group question_answer">
                            <label class="control-label">Select country</label>
                            <select  type="text" name="country" class="form-control" placeholder="Enter Answer">
                                <option>Select Answer</option>
                                <?php $countries = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_country WHERE (deleted_at is null or deleted_at = "")  AND status LIKE %d',1));
                                foreach($countries as $country): ?>
                                    <option name="countryName"  id="<?php echo $country->id;?>" value="<?php echo $country->name;?>"><?php echo $country->name;?></option>
                                <?php   endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group question_answer">
                            <label class="control-label">Select State</label>
                            <select type="text" name="state" class="form-control Select_state" placeholder="Select Answer">
                                <option >Select Answer</option>
<!--                                --><?php //$state = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_state WHERE (deleted_at is null or deleted_at = "")  AND status LIKE %d',1));
//                                foreach($state as $st): ?>
<!--                                    <option  name="stateName"  value="--><?php //echo $st->name;?><!--" id="--><?php //echo $st->id;?><!--" >--><?php //echo $st->name;?><!--</option>-->
<!--                                --><?php //  endforeach;
//                                ?>
                            </select>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="view-products errordiv">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="view-products">
                            <a class="select_product" data-toggle="tab">Select Products</a>
                        </div>
                    </div>
                </div>

            </div>
            <div id="productselect" class="tab-pane fade">
                <div class=" row question_part ">
                    <div class="col-sm-12">
                        <div class="bs-example ">
                            <div class="panel-group product_select" id="accordion">
                                <form id="category_tab">

                                  <?php  $questions = $wpdb->get_results('SELECT DISTINCT category_id FROM tbl_quotation_category_machine LEFT JOIN tbl_category ON (tbl_quotation_category_machine.category_id = tbl_category.id) WHERE (tbl_category.deleted_at is null or tbl_category.deleted_at = "")');?>
                                    <?php foreach($questions as $q):
									// $cat = $wpdb->get_col('SELECT  `category_name` FROM `tbl_category` WHERE `id` ='. $q->category_id);
										 // $cat = $wpdb->get_results($wpdb->prepare('SELECT `category_name` FROM `tbl_category` WHERE `status` LIKE %d AND (deleted_at is null or deleted_at = "")',1));	
                                    $cat =  $wpdb->get_col('SELECT  `category_name` FROM `tbl_category` WHERE `id` ='. $q->category_id.' AND `status`= 1 AND (deleted_at is null or deleted_at = "")');   
									//$cat = $wpdb->get_col('SELECT  `category_name` FROM `tbl_category` WHERE `id` ='. $q->category_id);
                                        //print_r( $cat[0]);
                                   ?>
                                    <?php ///$options = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_category WHERE status LIKE %d AND (deleted_at is null or deleted_at = "")',1));
                                    //foreach($options as $option):  ?>
                                    <div class="panel panel-default category_list_panel">
                                    <div class="panel-heading">
                                        <h4 class="panel-title panel-title-color">
                                            <a class="category_name" id="<?php echo $q->category_id;?>" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $q->category_id;?>"><?php echo $cat[0]?></a>
                                            <span class="error-msg" style="color:red;"></span>
                                        </h4>
                                    </div>
                                    <div id="collapse_<?php echo $q->category_id;?>" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="row">

                                                    <?php $id =  $q->category_id;
                                                    $results = $wpdb->get_results($wpdb->prepare('SELECT machine_id FROM  tbl_quotation_category_machine WHERE category_id Like %d',$id));
                                                    foreach($results as $rs):
                                                    //if( sizeof($rs) > 0):  ?>
                                                                <?php
                                                        $query ='SELECT machine_name FROM  tbl_machine WHERE id LIKE '.$rs->machine_id;
                                                        $machines = $wpdb->get_col($query,0);
                                                               foreach($machines as $key):  //print_r($key); ?>

                                                                    <div class="col-sm-6 col-xs-12" id="<?php echo $id;?>">
                                                                        <div class="checkbox checkbox-info select_product_checkbox" id="<?php echo $cat[0]?>">
                                                                            <input type="checkbox" name="checkbox" id="<?php echo $rs->machine_id; ?>" value="<?php  echo $key; ?>" >
                                                                            <label for="checkbox1" name="<?php  echo $key; ?>" name="<?php  echo $key; ?>" >
                                                                                <?php  echo $key; ?>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                <?php endforeach;  ?>
                                                        <?php   //endif;  ?>

                                                    <?php endforeach;  ?>


                                            </div>

                                        </div>
                                    </div>
                                    </div>
                                <?php   endforeach;
                                ?>

                                </form>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="view-products">
                            <a  id="submit_request_button" data-toggle="tab">Request Quotation</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="confirm_submit" class="tab-pane fade">
                <div class="row question_part product_select">
                    <div class="col-sm-8">
                        <div class="answerview">

                            <div class="head_question">
                                <label class="control-label">Quotations</label>
                                <hr class="label_border" />
                            </div>
                            <div class="question_rew">

                            </div>


                        </div>
                        <div class="form-group question_answer_review">
                            <div class="head_question">
                                <label class="control-label">Region</label>
                                <hr class="label_border" />
                            </div>

                            <div class="row">
                                <div class="col-sm-6 col-xs-12 ">
                                    <div class="question_answer ">
                                        <label class="control-label">Country</label>
                                        <span class="answer_span" id="country"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="question_answer ">
                                        <label class="control-label">State</label>
                                        <span class="answer_span" id="state"></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="head_question">
                            <label class="control-label">Select Product</label>
                            <hr class="label_border" />
                        </div>

                        <div class="clearfix"></div>
                        <div class="question_answer selected_cat">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="view-products">
                            <a href="javascript:void(0)" class="edit_button" data-toggle="tab" >Edit</a>
                            <a href="javascript:void(0)" class="submit_data">Submit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="clearfix"></div>


