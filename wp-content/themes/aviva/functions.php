<?php
/**
 * Aviva functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

/**
 * Aviva only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'aviva_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own aviva_setup() function to override in a child theme.
 *
 * @since Aviva 1.0
 */
function aviva_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/aviva
	 * If you're building a theme based on Aviva, use a find and replace
	 * to change 'aviva' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'aviva' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Aviva 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	//set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'aviva' ),
		'primary2' => __( 'Primary Menu2', 'aviva' ),
		'social'  => __( 'Social Links Menu', 'aviva' ),
		'footer'  => __( 'Footer Menu', 'aviva' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

    /*
     * Enable support for Page excerpt.
     *
     * See: https://codex.wordpress.org/Post_Formats
     */
    add_post_type_support( 'page', 'excerpt' );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, icons, and column width.
     */
    add_editor_style( 'css/editor-style.css' );
}
endif; // aviva_setup
add_action( 'after_setup_theme', 'aviva_setup' );




///**
// * Sets the content width in pixels, based on the theme's design and stylesheet.
// *
// * Priority 0 to make it available to lower priority callbacks.
// *
// * @global int $content_width
// *
// * @since Aviva 1.0
// */
//function aviva_content_width() {
//	$GLOBALS['content_width'] = apply_filters( 'aviva_content_width', 840 );
//}
//add_action( 'after_setup_theme', 'aviva_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Aviva 1.0
 */
function aviva_widgets_init() {

	/**	
	register_sidebar( array(
		'name'          => __( 'Sidebar1', 'aviva' ),
		'id'            => 'sidebar-1.',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'aviva' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) ); 
    **/
	register_sidebar( array(
		'name'          => __( 'Get In Touch', 'aviva' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'aviva' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 2', 'aviva' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears at the FOOTER.', 'aviva' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
    register_sidebar( array(
        'name'          => __( 'Phone Calls', 'aviva' ),
        'id'            => 'sidebar-4',
        'description'   => __( 'This Widget will be Appears at HEADER , you can add here PHONE NUMBER.The content will be managed by HTML.', 'aviva' ),
        'before_widget' => '<section  class="social-icons nav navbar-nav fr">',
        'after_widget'  => '</section>',
        'before_title'  => '',
        'after_title'   => '',
    ) );

    register_sidebar( array(
        'name'          => __( 'Email', 'aviva' ),
        'id'            => 'sidebar-5',
        'description'   => __( 'This Widget will be Appears at HEADER , you can add here EMAIL ADDRESS.The content will be managed by HTML.', 'aviva' ),
        'before_widget' => '<div class="social-icons nav navbar-nav fr">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => '',
    ) );
    register_sidebar( array(
        'name'          => __( 'Download Brochure', 'aviva' ),
        'id'            => 'sidebar-6',
        'description'   => __( 'This Widget will be Appears at HEADER, you can add here DOWNLOAD BROCHURE link.The content will be managed by HTML.', 'aviva' ),
        'before_widget' => '<div class="">',
        'after_widget'  => '</div>',
        'before_title'  => '',
        'after_title'   => 's',
    ) );
    register_sidebar( array(
        'name'          => __( 'Office Address', 'aviva' ),
        'id'            => 'sidebar-7',
        'description'   => __( 'This Widget will be Appears at CONTACT US page, you can add here OFFICE ADDRESS.The content will be managed by HTML.', 'aviva' ),
        'before_widget' => '<div class="office-detail">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Factory Address', 'aviva' ),
        'id'            => 'sidebar-8',
        'description'   => __( 'This Widget will be Appears at CONTACT US page, you can add here FACTORY ADDRESS.The content will be managed by HTML.', 'aviva' ),
        'before_widget' => '<div class="factory-detail">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Contact Person', 'aviva' ),
        'id'            => 'sidebar-9',
        'description'   => __( 'This Widget will be Appears at CONTACT US page ,You need to add TITLE  as a CONTACT PERSON and add detail as a Content.', 'aviva' ),
        'before_widget' => '<div class="title_display">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2><span>',
        'after_title'   => '</span></h2>',
    ) );
	register_sidebar( array(
        'name'          => __( 'Infrastructure', 'aviva' ),
        'id'            => 'sidebar-10',
        'description'   => __( 'This Widget will be Appears at About US page ,You need to add TITLE and add detail as a Content.', 'aviva' ),
        'before_widget' => '<div class="infrastructure">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );
	register_sidebar( array(
        'name'          => __( 'research-wing', 'aviva' ),
        'id'            => 'sidebar-11',
        'description'   => __( 'This Widget will be Appears at About US page ,You need to add TITLE and add detail as a Content.', 'aviva' ),
        'before_widget' => '<div class="research-wing">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );
	register_sidebar( array(
        'name'          => __( 'Paradigms of Quality', 'aviva' ),
        'id'            => 'sidebar-12',
        'description'   => __( 'This Widget will be Appears at About US page ,You need to add TITLE and add detail as a Content.', 'aviva' ),
        'before_widget' => '<div class="paradigms-details fl">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );


}
add_action( 'widgets_init', 'aviva_widgets_init' );
/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';




/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Aviva 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function aviva_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list'; 

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'aviva_widget_tag_cloud_args' );

/**
 *
 *
 * Adds excerpt to a page.
 * @param  integer $num Arguments for excerpt
 * @since Aviva 1.0
 */

function excerpt_aviva($num) {
    $limit = $num+1;
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt)."<br/><a class='service-item-link' href=".get_site_url()."/aviva/about-us/'>Read More  <i class=\"fa fa-caret-right\" aria-hidden=\"true\"></i></a>";
    echo $excerpt;
}

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Aviva 1.0
 */
function aviva_scripts() {
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( is_singular() && wp_attachment_is_image() ) {
        wp_enqueue_script( 'aviva-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160816' );
    }

    wp_enqueue_script( 'aviva-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160816', false );

    wp_localize_script( 'aviva-script', 'screenReaderText', array(
    'expand'   => __( 'expand child menu', 'aviva' ),
    'collapse' => __( 'collapse child menu', 'aviva' ),
) );
}
add_action( 'wp_enqueue_scripts', 'aviva_scripts' );

/*function new_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/','/ class="dropdown-menu dropdown-menu-left" /',$menu);
    return $menu;
}*/
//menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23

/*add_filter('wp_nav_menu','new_submenu_class');*/

function my_trim_excerpt( $text, $length = 55, $more = ' [&hellip;]'  )
{
    $text = strip_shortcodes( $text );
    $text = apply_filters( 'the_content', $text );
    $text = str_replace(']]>', ']]&gt;', $text);

    $excerpt = wp_trim_words( $text, $length, $more );
    return $excerpt;
}

function my_trim_title_excerpt( $text, $length = 55  )
{
    $text = strip_shortcodes( $text );
    $text = apply_filters( 'the_content', $text );
    $text = str_replace(']]>', ']]&gt;', $text);

    $excerpt = wp_trim_words( $text, $length );
    return $excerpt;
}

add_filter( 'post_type_link', 'my_post_type_link' );
function my_post_type_link( $post_link, $post = null ) {
    if ( !empty($post) ) {
        return str_replace('%post_id%', $post->ID, $post_link);
    }
}

