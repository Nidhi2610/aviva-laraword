jQuery(document).ready( function(e) {
	var path = window.location.origin;	
    var root = path+'/wp-content/themes/aviva/includes/category-list.php';
    jQuery.ajax({
        type: 'POST',
        url:  root,
        success: function (response) {
            var category_list = response;
            jQuery('.product-list').append(category_list);
        },
        error: function (xhr, status, error) {
            // check status && error
            console.log(status);
            console.log(error);
        }

    });


    jQuery(document).on('click',".category_items",function(){
            var parent = jQuery(".category_items").closest(".category_li");
            var id = (parent.attr('id'));
        jQuery("#category_name_"+id).addClass("custom-active");
    });









    var questions;
    var allQuestion;
    /* this function is get all questions of first_tab*/
    var result = {};
    function showValues() {
         result = {};
         allQuestion = jQuery( "#question_tab" ).serializeArray();
        jQuery.each(jQuery('#question_tab label'),function (index,item) {
            result[index] = jQuery(item).text();
        });
    }


        jQuery("select[name='country']").click(function(){
        var con  = jQuery(this).val();
        var i = jQuery(this).children().find("option[value='"+con+"']");
        var root = path+'/admin/public/api/v1/country_list';
        jQuery.ajax({
            type: 'GET',
            url:  root,
            success: function (response) {
                jQuery.each(response.data,function(i,item){
                    if(item.name == con ){
                        str='<option >Select Answer</option>';
                        jQuery.each(item.states ,function(index,s_item){
                            str+='<option name="stateName" value="'+ s_item.name +'" id="'+ s_item.id+'">'+s_item.name+'</option>';

                        });
                        jQuery(".Select_state").html(str);
                    }

                });
               //console.log(con);

            },
            error: function (xhr, status, error) {
               console.log( error);
            }
        });
    });


        jQuery( "input[type='text']" ).on( "click", showValues );
        jQuery( "select" ).on( "change", showValues );
    var state_id;
    var state;
    var country;
    var country_id;
    var errorFlag = 0;

    jQuery('.select_product').click(function(){    /* for submit the first_tab*/
        var state_val;
        var country_val;
         questions = '';

        // get state_id and country_id form first_tab
        state_val =(jQuery("select[name='state']").val());
        state_id= jQuery("option[value='"+ state_val +"']").attr('id');

        country_val =(jQuery("select[name='country']").val());
        country_id= jQuery("option[value='"+ country_val +"']").attr('id');


        showValues();
        var data = [];
        function appendQuestion(){
             data = [];
            var html= '';
            jQuery.each(allQuestion.slice(0,-2),function(i,answer){
                data += ('{"question":"'+result[i]+'","answer":"'+ answer.value +'"},' );  //store all values in one array
                 html += ' <div class="form-group question_answer question_answer_review"> <h3 class="control-label ">'+result[i]+'</h3><span class="answer_span">'+answer.value+'</span></div>';
            });
            jQuery(".question_rew").html(html);     // append the question on review_tab
        }

        jQuery.each(allQuestion,function(i,answer){

            if( (answer.value === "Select Answer")||(answer.value === " ")){
                errorFlag=1;

            }
            else{}

        });

        if(errorFlag){
            jQuery(".errordiv").text("**All Fields are Required. Please fill all the Fields.");
            errorFlag = 0;
        }else{
            appendQuestion();

            questions = "[" + data.slice(0,-1) + "]"; // formatting for pass in api
            var len = allQuestion.length;  // get length of all question of first tab
            state =allQuestion[len-1];
            country =allQuestion[len-2];
            // console.log(questions);
            jQuery('#state').text(state.value);   // append the state on last review_tab
            jQuery('#country').text(country.value); // append the country on last review_tab

            jQuery(".select_product_tab").addClass("active");
            jQuery(".first_tab").removeClass("active");
            jQuery("#submit_request").removeClass("active");
            jQuery(this).attr('href','#productselect');


        }
  });


/* second_tab for select categories and products*/
    var checked = {};
    var cat_name = [];
    var category;

    /* for getting array of value(name) of checkbox and value(name) of related category name */
    function getValues() {
        jQuery.each(jQuery("input[class='checked']"), function (index, item) {
            checked[index] = jQuery(item).attr('value');     // for getting value(name) of checkbox
            category = jQuery(item).closest(".select_product_checkbox").attr('id');
                  if(jQuery.inArray(category,cat_name) <= 0){
                      cat_name.push(category);      // array of category_name of checked[index]  product
                  }
        });

    }

    /*for check and uncheck the checkboxes*/ checked="checked"
    jQuery('label[for="checkbox1"]').click(function(){

        if( jQuery(this).siblings().attr('checked')){
            jQuery(this).siblings().attr('checked',false);
            jQuery(this).siblings().removeClass('checked');

        }else{
            //jQuery(this).closest('input[name="checkbox"]').addClass('checked');
            jQuery(this).siblings().attr('checked',true);
            jQuery(this).siblings().addClass('checked');
            getValues();
            }
    });

    var alertFlag=1;

   // var categoryNameArray = [];
    var firstFlag =  false;
    var secondFlag =  false;


    var j=0;



    /* for submit the submit_request button for submit the second_tab*/
    var cateGory_ID=[];
    jQuery('#submit_request_button').click( function () {

        var results = {};
        var cat_pro = [];
        var categoryArray = [];
        var categoryNameArray = [];
        var categoryName = [];
        var productName = [];
        var product = [];
        var categoryIdCheck = 0;
        var categoryNameCheck = 0;
        /*for getting array contains names of category and related product  to display on review_tab*/
        jQuery.each(jQuery("input[class='checked']"), function (index, item) {

            categoryName = jQuery(this).closest('.select_product_checkbox').attr('id');
            productName = jQuery(this).attr('value');
            if (categoryNameCheck != categoryName) {
                if (secondFlag) {
                    categoryNameArray.push(product);
                    product = [];
                }
                categoryNameCheck = categoryName;
                product['category_name'] = categoryName;
                product['machine_name'] = [];
                product['machine_name'].push(productName);
                secondFlag = true;

            } else {
                product['machine_name'].push(productName);
            }

            if ((index + 1) == jQuery("input[class='checked']").length) {
                categoryNameArray.push(product);
                product = [];
            }


            /*for getting array contains ids of category and related product */
            var productId = jQuery(this).attr('id');
            var categoryId = jQuery(this).closest("div[class='col-sm-6 col-xs-12']").attr('id');

            if (categoryIdCheck != categoryId) {
                if (firstFlag) {
                    categoryArray.push(cat_pro);
                    cat_pro = [];
                }
                categoryIdCheck = categoryId;
                cat_pro['category_id'] = categoryId;
                cat_pro['machine_id'] = [];
                cat_pro['machine_id'].push(productId);
                firstFlag = true;

            } else {
                cat_pro['machine_id'].push(productId);
            }

            if ((index + 1) == jQuery('input[class="checked"]').length) {
                categoryArray.push(cat_pro);
              //  console.log("categoryArray" +categoryArray);
                cat_pro = [];
            }
            j++;
          //  console.log("categoryArray:" +categoryArray);
        });
       // console.log(categoryArray);
        function appendVal() {

            /*for append the name of categories and products on review_tab*/
            var selected_cat_html = '';
            jQuery.each(categoryNameArray, function (index, item) {

                if (typeof(item.category_name) !== "undefined") {
                    var pro_html = '';
                    if (jQuery.isArray(item.machine_name)) {
                        jQuery.each(item.machine_name, function (indexno, itemname) {
                            pro_html += '<label class="product_review">' + itemname + '</label>';
                        });
                    }

                   selected_cat_html += '<div class="panel panel-default new_cate_' + index + '">'
                        + '<div class="panel-heading"><h4 class="panel-title"><label>' + item.category_name + '</label></h4></div>'
                        + '<div class="panel-body"><div class="row"><div class="col-sm-12 cat_name_' + index + '">' + pro_html + '</div>'
                        + '</div></div></div>';
                   
                }
            });

            jQuery(".selected_cat").html(selected_cat_html);

        }
        var flag=0;
        var lengthofcat=0;

        jQuery.each(jQuery(".category_list_panel"), function(){
            lengthofcat++;
            //console.log( lengthofcat);
            var div = jQuery(this).children().find('input');
            jQuery(this).children().css('box-shadow', '0 0px 0px');
            jQuery(this).children().css('border', '0px');


            if(div.hasClass("checked")){
                flag++; console.log( flag);
            }else{
                flag;
                jQuery(this).children().css('box-shadow', '0 1px 1px red');
                jQuery(this).children().css('border', '2px solid red');
            }
        });
        
        if(flag < lengthofcat){
            jQuery("#submit_request_button").attr('href', 'javascript:void(0)');
           // console.log("in if");
        }else{
           // console.log("in else");
            appendVal();
            jQuery("#submit_request_button").attr('href', '#confirm_submit');
            jQuery("#submit_request").addClass("active");
            jQuery(".select_product_tab").removeClass("active");
            jQuery(".first_tab").removeClass("active");
        }

        cateGory_ID=[];



        console.log(categoryArray);

        jQuery.each(categoryArray, function (index, item) {
           // console.log("Before loop" + cateGory_ID);
            if(typeof(item.category_id) !== "undefined"){
                cateGory_ID += '{"category_id"' + ':' + item.category_id + ',"machine_id":[' + item.machine_id + '] }' + ',';

            }
           // console.log("after loop" + cateGory_ID);
        });
    });


    /* for getting the user_id of logged_in user*/
    var user_info = localStorage.getItem('user_id');
    var user_api_token = localStorage.getItem('user_api_token');

    jQuery(".submit_data").click(function(){
        /* proper formating for passing in ajax*/

        var selected_category;
        selected_category = "[" + cateGory_ID.slice(0,-1) + "]"; // formatting for pass in api

        console.log("cat_ids" +cateGory_ID  );

        console.log("userid"+user_info);
        console.log("api"+user_api_token);
       console.log("questions"+questions);
        console.log("formated array"+selected_category); // array of category + product id
        console.log("c_id"+country_id);
        console.log("s_id"+state_id);
        var path = window.location.origin;	
        var root = path+"/admin/public/api/v1/request_quotation";
        jQuery.ajax({
            data:{
                "country_id":country_id,
                "state_id":state_id,
                "user_id":user_info,
                "questions":questions,
                "selected_category":selected_category,
                "api_token":user_api_token
            },
            type: 'POST',
            url:  root,
            success: function (response) {
                alert(response.message);
                window.location.href=path;
            },
            error: function (xhr, status, error) {
              alert( error);
            }

        });
    });

    jQuery(".edit_button").click(function(){

        jQuery.each(jQuery("input[class='checked']"), function (index,item) {
            jQuery(this).removeClass("checked");
            jQuery(this).removeAttr("checked");
        });



        cateGory_ID=[];

        jQuery(".first_tab").addClass("active");
            jQuery(".select_product_tab").removeClass("active");
            jQuery("#submit_request").removeClass("active");

        jQuery(this).attr('href','#basic_info');

    });


});


