<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */

include('../../../../wp-load.php');

    global $category_list;
    $category_list .= '';
$results = $wpdb->get_results($wpdb->prepare('SELECT * FROM tbl_category WHERE (deleted_at is null or deleted_at = "") AND status LIKE %d ', 1));
    $category_list .='';

   if ($results) {
       $category_list .='<ul class="dropdown-menu dropdown-menu-left">';
       foreach($results as $result){
           $slug = sanitize_title_with_dashes($result->category_name);
           $category_list .= '<li class="category_li" id="'.$result->id.'"><a href=" '. get_site_url().'/category-products?id='. $result->id .'&&'.$slug.'" class="category_items" >'. $result->category_name .'</a></li>';
       }
       $category_list .= '</ul>';
       }

$category_list .='';
   echo $category_list;
?>