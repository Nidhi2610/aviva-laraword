<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Aviva 1.0
 */
?>

<div class="footer fl">
    <div class="footer_main">
        <div class="footer_address">
            <div class="footer-info fr">
                <?php
                if(is_active_sidebar('sidebar-2')){
                    dynamic_sidebar('sidebar-2');
                }
                ?>
                <div class="social-icon fl">
                    <?php if ( has_nav_menu( 'social' ) ) : ?>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'social',
                        ) );
                        ?>
                    <?php endif;?>
                </div>


                <div class="copyright fl">
                    <span>© 2018 All Rights Reserved</span>
                </div>

            </div>
        </div>
        <div class="footer_form">

                <?php echo do_shortcode('[contact-form-7 id="52" title="Contact form 1"]');?>
            </div>
        </div>
    </div>
    <div class="footer-menu fl">
        <div class="wrap">
            <?php if ( has_nav_menu( 'footer' ) ) : ?>
                <?php
                wp_nav_menu( array(
                    'menu_class'     => 'wrap',
                    'container'      => '',
                    'link_'    =>'',
                    'link_after'    => '<img src="'. get_template_directory_uri().'/images/oblick.png" alt="oblick">',
                    'theme_location' => 'footer',
                    'depth'  		 =>0,
                ) );
                ?>
            <?php endif;?>
        </div>
    </div>
</div>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.validation.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/owl.carousel.min.js" type="text/javascript"></script>
<script>
    window.onload = function(){
        if(navigator.userAgent.match(/iPhone|iPod/i)){
            jQuery('.sign-in').css('display','none');
        }
    }
</script>
<script src="<?php echo get_template_directory_uri();?>/js/default.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/includes/ajax-call.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/includes/login/login.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/test.js" type="text/javascript"></script>


<script>
	
	function isNumber(evt,EleName) {
					jQuery('.your-number').children('.wpcf7-not-valid-tip').remove();
					evt = (evt) ? evt : window.event;
					var charCode = (evt.which) ? evt.which : evt.keyCode;	
					if (charCode > 31 && (charCode < 48 || charCode > 57)) {							
						var sp=	jQuery('<span></span>').insertAfter(EleName);							    						sp.attr('class','wpcf7-not-valid-tip');
							sp.html('Enter only digit.');						
							return false;
	// 					alert('please enter numbers only');
	// 					return false;
					}
					return true;					
				}
				jQuery("#contact_number").on('keyup', function(event){
					var EleName = jQuery("#contact_number");
					   return isNumber(event,EleName);  
					});
				jQuery("#contact_number2").on('keyup', function(event){
					var EleName2 = jQuery("#contact_number2");
					   return isNumber(event,EleName2);  
					});
				
    jQuery('.wpcf7-submit').click(function(e){		
        var len = jQuery('#contact_number').val().length; 
        if(len === 0){
            return true;
        }else{
            if(len !== 10){ 
				jQuery('.your-number').children('.wpcf7-not-valid-tip').remove();
               var sp=	jQuery('<span></span>').insertAfter(jQuery('#contact_number'));
               sp.attr('class','wpcf7-not-valid-tip');
               sp.html('Enter 10 digit contact number.');
                return false;
            }else{
                document.addEventListener( 'wpcf7mailsent', function( event ) {
                    var path = window.location.href.split('/').slice(0, 5).join('/');
                    location = path;
                }, false );
            }
        }
    });

    jQuery('#contact_submit').click(function(e){			
		
        var len = jQuery('#contact_number2').val().length;		
        if(len === 0){
            return true;
        }else{ 
            if(len !== 10){ 
				
				jQuery('.your-number').children('.wpcf7-not-valid-tip').remove();				
					
						var sp=	jQuery('<span></span>').insertAfter(jQuery('#contact_number2'));
					
						sp.attr('class','wpcf7-not-valid-tip');
						sp.html('Enter 10 digit contact number.');
					
                
                return false;
            }else{
                document.addEventListener( 'wpcf7mailsent', function( event ) {
                    var path = window.location.href.split('/').slice(0, 5).join('/');
                    location = path;
                }, false );
            }
        }
    });
	
jQuery(document).ready(function() {	

	jQuery('div.wpcf7-response-output').remove();
	jQuery("div.wpcf7-validation-errors").removeAttr('class','wpcf7-validation-errors'); 
	
});


jQuery("#contact_submit").click( function(){
// jQuery(".wpcf7-validation-errors").css("border-style","none"); 
	jQuery("div.wpcf7-validation-errors").removeAttr('class','wpcf7-validation-errors'); 
});

jQuery(document).ready(function() {
//jQuery("div.wpcf7-mail-sent-ng").css("border-style","none");
});
</script>
<?php wp_footer(); ?>
</body>
</html>
